<?php
/**
 * @file
 * dfs_pm_roles.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function dfs_pm_roles_user_default_roles() {
  $roles = array();

  // Exported role: Client.
  $roles['Client'] = array(
    'name' => 'Client',
    'weight' => 12,
    'machine_name' => 'pm_client',
  );

  // Exported role: Developer.
  $roles['Developer'] = array(
    'name' => 'Developer',
    'weight' => 11,
    'machine_name' => 'pm_developer',
  );

  // Exported role: HR.
  $roles['HR'] = array(
    'name' => 'HR',
    'weight' => 9,
    'machine_name' => 'pm_hr',
  );

  // Exported role: Project Manager.
  $roles['Project Manager'] = array(
    'name' => 'Project Manager',
    'weight' => 10,
    'machine_name' => 'pm_project_manager',
  );

  // Exported role: Tester.
  $roles['Tester'] = array(
    'name' => 'Tester',
    'weight' => 13,
    'machine_name' => 'pm_tester',
  );

  return $roles;
}
