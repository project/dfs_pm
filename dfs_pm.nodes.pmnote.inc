<?php

/**
 * @file
 *  Migration for Drupal PM Notes.
 */


/**
 * Implementation of DFSPMNodes, to support migration of Notes.
 */
class DFSPMNotes extends DFSPMNodes {

  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Import Notes.');
    $this->destination = new MigrateDestinationNode('pmnote');
    $this->addfieldmapping('pmnote_parent', 'parent')->sourceMigration($arguments['parent_source_class_name']);
  }

  function csvcolumns() {
    $columns[0] = array('dfs_pm_key', 'dfs_pm_key');
    $columns[1] = array('title', 'Name');
    $columns[2] = array('author', 'Author');
    $columns[3] = array('parent', 'Parent');
    $columns[4] = array('body', 'Note');
    return $columns;
  }

}
