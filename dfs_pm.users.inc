<?php

/**
 * @file
 *  Migration for Commerce Products used in DFS WEM.
 */

class DFSPMUsers extends DFUsers {
  public function __construct($arguments) {
    $arguments['path'] = dfs_pm_import_path($arguments['filename']);
    parent::__construct($arguments);
    $this->description = t('Import Users.');
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'dfs_pm_key' => array(
          'type' => 'char',
          'length' => 36,
          'not null' => FALSE,
        ),
      ),
      MigrateDestinationNode::getKeySchema()
    );
  }

  function csvcolumns() {
    $columns[0] = array('dfs_pm_key', 'dfs_pm_key');
    $columns[1] = array('name', 'name');
    $columns[2] = array('pass', 'pass');
    $columns[3] = array('mail', 'mail');
    $columns[4] = array('status', 'status');
    $columns[5] = array('roles', 'roles');
    $columns[6] = array('picture', 'picture');
    return $columns;
  }

  public function prepareRow($row) {
    parent::prepareRow($row);
    return TRUE;
  }
}


class DFSPMUserPictures extends ImportBaseUserPictures {
  public function __construct($arguments) {
    $import_path = $arguments['path'] = dfs_pm_import_path($arguments['filename']);
    parent::__construct($arguments);
    $this->source = new MigrateSourceCSV($import_path, $this->csvcolumns(), array('header_rows' => 1));
    // Get the base path, which is where we assume /images is located
    $base_path = dirname($import_path);
    $this->addFieldMapping('source_dir')->defaultValue($base_path . '/images');
  }

  function csvcolumns() {
    $columns[6] = array('picture', 'Picture');
    return $columns;
  }
}
