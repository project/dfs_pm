<?php

/**
 * @file
 *  Migration for Drupal PM Organization nodes.
 */


/**
 * Implementation of ImportBaseNodes, to support migration of moderated nodes.
 */
class DFSPMIssueBase extends DFSPMNodesWithDate {
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Import Issue Type Nodes.');

    $this->addfieldmapping('pm_assigned', 'assigned')->sourceMigration('DFSPMUsers');
    $this->addfieldmapping('pm_manager', 'manager')->sourceMigration('DFSPMUsers');

    $this->addfieldmapping('pm_duration', 'duration');
    $this->addfieldmapping('pm_durationunit', 'duration_unit');
    $this->addfieldmapping('pm_category', 'category');
    $this->addfieldmapping('pm_priority', 'priority');
    $this->addfieldmapping('pm_status', 'task_status');
    $this->addfieldmapping('pm_step_number', 'step_number');
    $this->addfieldmapping('pm_weight', 'weight');
    $this->addfieldmapping('pm_billing_status', 'billing_status');
    $this->addfieldmapping('pm_currency', 'currency');
    $this->addfieldmapping('pm_price', 'price');
    $this->addfieldmapping('pm_pricemode', 'price_mode');

  }

  function csvcolumns() {
    $columns = array();
    $columns[0] = array('dfs_pm_key', 'dfs_pm_key');
    $columns[1] = array('title', 'Name');
    $columns[2] = array('author', 'Author');
    $columns[3] = array('assigned', 'Assigned');
    $columns[4] = array('category', 'Category');
    $columns[5] = array('date', 'Date');
    $columns[6] = array('parent', 'Parent');
    $columns[7] = array('priority', 'Priority');
    $columns[8] = array('task_status', 'Task Status');
    $columns[9] = array('step_number', 'Step Number');
    $columns[10] = array('weight', 'Weight');
    $columns[11] = array('billing_status', 'Billing Status');
    $columns[12] = array('currency', 'Currency');
    $columns[13] = array('duration', 'Duration');
    $columns[14] = array('duration_unit', 'Duration Unit');
    $columns[15] = array('price', 'Price');
    $columns[16] = array('price_mode', 'Price Mode');
    $columns[17] = array('body', 'Note');
    return $columns;
  }

}

/**
 * Implementation of DFSPMIssueBase, to support migration of moderated nodes.
 */
class DFSPMTasks extends DFSPMIssueBase {
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->destination = new MigrateDestinationNode('pmtask');
    $this->addfieldmapping('pmtask_parent', 'parent')->sourceMigration('DFSPMProjects');
  }
}

/**
 * Implementation of DFSPMIssueBase, to support migration of moderated nodes.
 */
class DFSPMTickets extends DFSPMIssueBase {
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->destination = new MigrateDestinationNode('pmticket');
    $this->addfieldmapping('pmticket_parent', 'parent')->sourceMigration('DFSPMProjects');
  }
}

/**
 * Implementation of DFSPMIssueBase, to support migration of moderated nodes.
 */
class DFSPMIssues extends DFSPMIssueBase {
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->destination = new MigrateDestinationNode('pmissue');
    $this->addfieldmapping('pmissue_parent', 'parent')->sourceMigration('DFSPMProjects');
    $this->addfieldmapping('pmissue_parent_issue', 'parent_issue')->sourceMigration('DFSPMIssues');
    $this->addfieldmapping('pmissue_related', 'related_issue')->sourceMigration('DFSPMIssues');
  }

  function csvcolumns() {
    $columns = array();
    $columns[0] = array('dfs_pm_key', 'dfs_pm_key');
    $columns[1] = array('title', 'Name');
    $columns[2] = array('author', 'Author');
    $columns[3] = array('assigned', 'Assigned');
    $columns[4] = array('category', 'Category');
    $columns[5] = array('date', 'Date');
    $columns[6] = array('parent', 'Parent');
    $columns[7] = array('parent_issue', 'Parent Issue');
    $columns[8] = array('related_issue', 'Related Issue');
    $columns[9] = array('priority', 'Priority');
    $columns[10] = array('task_status', 'Task Status');
    $columns[11] = array('step_number', 'Step Number');
    $columns[12] = array('weight', 'Weight');
    $columns[13] = array('billing_status', 'Billing Status');
    $columns[14] = array('currency', 'Currency');
    $columns[15] = array('duration', 'Duration');
    $columns[16] = array('duration_unit', 'Duration Unit');
    $columns[17] = array('price', 'Price');
    $columns[18] = array('price_mode', 'Price Mode');
    $columns[19] = array('body', 'Note');
    return $columns;
  }

  public function prepareRow($row) {
    parent::prepareRow($row);
    $row->related_issue = explode(",", $row->related_issue);
    return TRUE;
  }
}
