<?php

/**
 * @file
 *  Migration for Drupal PM Organization nodes.
 */


/**
 * Implementation of ImportBaseNodes, to support migration of moderated nodes.
 */
class DFSPMOrganizations extends DFSPMNodes {
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Import Organizations.');
    // Create a MigrateSource object.
    $this->destination = new MigrateDestinationNode('pmorganization');
    $this->addfieldmapping('pm_active', 'active');
    $this->addfieldmapping('pm_phone', 'phone');
    $this->addfieldmapping('pm_mail', 'mail');
    $this->addfieldmapping('pm_currency', 'currency');
    $this->addfieldmapping('pm_price', 'price');
    $this->addfieldmapping('pm_pricemode', 'pricemode');
    $this->addfieldmapping('pm_taxid', 'taxid');
    $this->addfieldmapping('pm_www', 'www');
    $this->addfieldmapping('pmorganization_members', 'members')->sourceMigration('DFSPMUsers');
    $this->addfieldmapping('pmorganization_type', 'type');
    $this->addfieldmapping('pm_orglanguage', 'org_language');

    $this->addFieldMapping('pm_address', 'country');
    $this->addFieldMapping('pm_address:thoroughfare', 'thoroughFare');
    $this->addFieldMapping('pm_address:premise', 'premise');
    $this->addFieldMapping('pm_address:locality', 'locality');
    $this->addFieldMapping('pm_address:administrative_area', 'administrativeArea');
    $this->addFieldMapping('pm_address:postal_code', 'postal_code');

  }

  function csvcolumns() {
    $columns[0] = array('dfs_pm_key', 'dfs_pm_key');
    $columns[1] = array('title', 'Name');
    $columns[2] = array('country', 'country');
    $columns[3] = array('thoroughFare', 'thoroughFare');
    $columns[4] = array('premise', 'premise');
    $columns[5] = array('locality', 'locality');
    $columns[6] = array('administrativeArea', 'administrativeArea');
    $columns[7] = array('postal_code', 'postCode');
    $columns[8] = array('phone', 'Phone');
    $columns[9] = array('www', 'Website');
    $columns[10] = array('mail', 'E-mail');
    $columns[11] = array('currency', 'Currency');
    $columns[12] = array('pricemode', 'Price Mode');
    $columns[13] = array('price', 'Price');
    $columns[14] = array('taxid', 'Tax ID');
    $columns[15] = array('active', 'Active');
    $columns[16] = array('org_language', 'Language');
    $columns[17] = array('type', 'Type');
    $columns[18] = array('members', 'Members');
    $columns[19] = array('author', 'Author');
    $columns[20] = array('body', 'Note');
    return $columns;
  }

  public function prepareRow($row) {
    $row->members = explode(",", $row->members);
    return TRUE;
  }

}
