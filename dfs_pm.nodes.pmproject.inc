<?php

/**
 * @file
 *  Migration for Drupal PM Organization nodes.
 */


/**
 * Implementation of ImportBaseNodes, to support migration of moderated nodes.
 */
class DFSPMProjects extends DFSPMNodesWithDate {
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Import Projects.');
    $this->destination = new MigrateDestinationNode('pmproject');

    $this->addfieldmapping('pm_assigned', 'assigned')->sourceMigration('DFSPMUsers');
    $this->addfieldmapping('pm_manager', 'manager')->sourceMigration('DFSPMUsers');

    $this->addfieldmapping('pm_duration', 'duration');
    $this->addfieldmapping('pm_durationunit', 'duration_unit');
    $this->addfieldmapping('pm_projectcategory', 'category');
    $this->addfieldmapping('pm_projectpriority', 'priority');
    $this->addfieldmapping('pm_projectstatus', 'project_status');
    $this->addfieldmapping('pmproject_parent', 'organization')->sourceMigration('DFSPMOrganizations');
    $this->addfieldmapping('pm_billing_status', 'billing_status');
    $this->addfieldmapping('pm_currency', 'currency');
    $this->addfieldmapping('pm_price', 'price');
    $this->addfieldmapping('pm_pricemode', 'price_mode');

  }

  function csvcolumns() {
    $columns[0] = array('dfs_pm_key', 'dfs_pm_key');
    $columns[1] = array('title', 'Name');
    $columns[2] = array('author', 'Author');
    $columns[3] = array('assigned', 'Assigned');
    $columns[4] = array('manager', 'Manager');
    $columns[5] = array('date', 'Date');
    $columns[6] = array('duration', 'Duration');
    $columns[7] = array('duration_unit', 'Duration Unit');
    $columns[8] = array('category', 'Category');
    $columns[9] = array('priority', 'Priority');
    $columns[10] = array('project_status', 'Project Status');
    $columns[11] = array('organization', 'Organization');
    $columns[12] = array('billing_status', 'Billing Status');
    $columns[13] = array('currency', 'Currency');
    $columns[14] = array('price', 'Price');
    $columns[15] = array('price_mode', 'Price Mode');
    $columns[16] = array('body', 'Note');
    return $columns;
  }
}
