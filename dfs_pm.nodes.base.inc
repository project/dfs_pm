<?php

/**
 * @file
 *  Migration for Drupal PM Organization nodes.
 */

/**
 * Implementation of ImportBaseNodes, to support migration of moderated nodes.
 */
class DFSPMNodes extends ImportBaseNodes {
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Import Nodes.');
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'dfs_pm_key' => array(
          'type' => 'char',
          'length' => 36,
          'not null' => FALSE,
        ),
      ),
      MigrateDestinationNode::getKeySchema()
    );

    // Create a MigrateSource object.
    $this->source = new MigrateSourceCSV(dfs_pm_import_path($arguments['filename']), $this->csvcolumns(), array('header_rows' => 1));

    $this->addFieldMapping('body:format')->defaultValue('full_html');
    // Created
    $this->addFieldMapping('created', 'created')->defaultValue(strtotime("now"));
    $this->addfieldmapping('uid', 'author')->sourceMigration('DFSPMUsers');
  }

  public function prepareRow($row) {
    parent::prepareRow($row);
  }
}

/**
 * Implementation of ImportBaseNodes, to support migration of moderated nodes.
 */
class DFSPMNodesWithDate extends DFSPMNodes {
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->addfieldmapping('pm_date', 'date');
    $this->addfieldmapping('pm_date:to', 'date_to');
  }

  public function prepareRow($row) {
    parent::prepareRow($row);
    $date = explode(' to ', $row->date);
    $row->date =(string) isset($date[0]) ? strtotime($date[0]) : '';
    $row->date_to =(string) isset($date[1]) ? strtotime($date[1]) : '';
    return TRUE;
  }
}
